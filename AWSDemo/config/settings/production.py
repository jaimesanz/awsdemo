from .base import *  # noqa: F403 F401

DEBUG = False
ALLOWED_HOSTS = ['*']

STATIC_ROOT = os.path.join(BASE_DIR, "static_root")
