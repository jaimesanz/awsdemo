#!/bin/bash
python3 manage.py migrate --settings=config.settings.production
python3 manage.py collectstatic --clear --noinput --settings=config.settings.production
python3 manage.py collectstatic --noinput --settings=config.settings.production
ln -s /code/mysite_nginx.conf /etc/nginx/sites-enabled/
service nginx start
service nginx status
# Prepare log files and start outputting logs to stdout
# Start Gunicorn processes
echo Starting uWSGI
exec uwsgi --socket :8001 --module config.wsgi