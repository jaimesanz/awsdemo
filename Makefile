# Determine this makefile's path.
# Be sure to place this BEFORE `include` directives, if any.
# source: https://stackoverflow.com/a/27132934/7331040
THIS_FILE := $(lastword $(MAKEFILE_LIST))
MANAGE = python AWSDemo/manage.py


# target: all - Default target. Does nothing.
all:
	@echo "Hello $(LOGNAME), nothing to do by default.";
	@echo "Try 'make help'.";


# target: dev - Builds the project using 'development' settings.
dev:
	pip install -r requirements/dev.txt;
	$(MANAGE) migrate;


# target: help - Display callable targets.
help:
	@echo "These are common commands used in various situations:\n";
	@grep -E "^# target:" [Mm]akefile;


# target: lint - Checks that python code follows pep8, has docstrings, and imports are sorted properly.
lint:
	@echo "pep8";
	@flake8 AWSDemo/* --max-complexity 10;
	@echo "docstrings";
	@pydocstyle AWSDemo --count;
	@echo "imports";
	@isort AWSDemo --recursive --check-only;


# target: qa - Runs all qa tools.
qa:
	@bandit -ll -r AWSDemo;
	@safety check -r requirements/base.txt -r requirements/dev.txt -r requirements/qa.txt
	@$(MAKE) -f $(THIS_FILE) lint;


# target: run - Runs dev server. You can pass additional arguments with ARGS parameter, eg: 'make run ARGS=9000'.
run:
	$(MANAGE) runserver_plus ${ARGS} || $(MANAGE) runserver ${ARGS};


# target: shell - Opens django shell.
shell:
	$(MANAGE) shell_plus || $(MANAGE) shell;
